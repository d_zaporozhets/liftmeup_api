const db = require('mongoose');
const Schema = db.Schema;

class Route {
  static createRoute(routeData) {
    return this.create({
      _id: new db.Types.ObjectId(),
      ...routeData
    });
  }
}

const routeSchema = new Schema({
  _id: { type: Schema.Types.ObjectId },
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  pointA: { type: String },
  pointB: { type: String },
  transitPoints: [{ type: String }],
  timestamp: { type: Date, default: Date.now },
  companions: [{ type: String }],
  auto: { number: String, seats: Number }
});

routeSchema.loadClass(Route);

module.exports = routeSchema;
