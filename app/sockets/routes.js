const Route = require('../models').Route;
const User = require('../models').User;
const notify = require('../sockets/notifications').notifyAll;

module.exports = (io) => {
  const routes = io.of('/routes');

  routes.on('connection', (socket) => {
    const on = io.listenerDecorator(socket);

    socket.use((packet, next) => {
      packet.push(socket.handshake.query.email.toLowerCase());
      return next();
    });

    on('save', async([data, email]) => {
      const user = await User.getByEmail(email);
      const route = await Route.createRoute({ ...data, owner: user._id });
      debugger;
      user.routes.push(route);
      user.save();
      await notify({
        body: 'testtet123',
        actions: [{
          name: 'TEst',
          payload: {
            path: `/route/${route._id}`
          }
        }]
      });
      return route;
    });

    on('get', async email => {
      const user = await User.getByEmail(email);
      return user.routes;
    });

    on('getById', async([id, ..._]) => {
      return Route.findById(id).exec();
    });
  });
};
